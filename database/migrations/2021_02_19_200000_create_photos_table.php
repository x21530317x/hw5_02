<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    public function up()
    {
	Schema::create('photos', function (Blueprint $table){
	    $table->increments('id');
	    $table->integer('message_id')->unsigned()->index();
	    $table->string('path');
	    $table->timestamps();
	});
    }

    public function down()
    {
        Schema::drop('photos');
    }
}