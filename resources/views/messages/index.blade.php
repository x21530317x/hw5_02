@extends('layouts.app')
@section('content')
    <!-- Bootstrap 樣板... -->
    <div class="panel-body">
        <!-- 顯示驗證錯誤 -->
        @include('common.errors')
        <!-- 新留言的表單 -->
    <div style='text-align:center'>
	    <h1 style="font-size:50px">二手商品交換區</h1>
	@if(Auth::user()->id==1)
	  	    <div style="float:left; padding-left:90%">
			<form action="{{ url('messages/report_list') }}" method="POST">
	@php $count=1 @endphp
	@foreach($messages as $message)
	@foreach($message->reports as $report)
	<div style="display:none">
		<textarea name='{{'report'.$count}}' class="form-control">{{ $report->reason }}</textarea>
		<textarea name='{{'reporter'.$count}}' class="form-control">{{ $report->reporter }}</textarea>
		<textarea name='{{'reported'.$count}}' class="form-control">{{ $report->reported }}</textarea>
		<textarea name='{{'message_id'.$count}}' class="form-control">{{ $report->message_id }}</textarea>
		<textarea name='{{'message_name'.$count}}' class="form-control">{{ $report->message_name }}</textarea>
		<textarea name='{{'create_at'.$count}}' class="form-control">{{ $report->created_at }}</textarea>
	</div>
	@php $count+=1 @endphp
	@endforeach
	@endforeach
	<div style="display:none">
		<textarea name='count' class="form-control">{{ $count-1 }}</textarea>
	</div>
		    <button type="submit" id="" class="btn btn-danger">
		        <i class="fa fa-btn fa-bell"></i>
		    </button>
			</form>
	    	    </div>
	@endif

	<form action="{{ url('messages/add') }}" method="POST">
		<div style="float:right">
		    <button type="submit" id="add" class="btn btn-info">
			發佈 <i class="fa fa-comment-o"></i>
		    </button>
		</div>
	</form>
    </div>

    <!-- 顯示目前留言 -->
@php $message_count=0 @endphp
@foreach($messages as $m)
@php $message_count+=1 @endphp
@endforeach
<!--{{$message_count}}文章數量-->
@php $rates=array(0); @endphp
@for($i=0 ; $i<=$message_count ; $i++)
@php $rates=array_pad($rates,$i,0); @endphp
@endfor
<!--
@for($i=0 ; $i<$message_count ; $i++){{$rates[$i]}}@endfor
-->
<!--$rates array評分 初始化-->
@php $i=1 @endphp
@foreach($messages as $m)
@if($m->stars->where('message_id',$i)->avg('stars')!="")
	@php $rates[$m->id-1] = $m->stars->where('message_id',$i)->avg('stars') @endphp
@else
	@php $rates[$m->id-1] = 0@endphp
@endif
@php $i+=1 @endphp
@endforeach
@php $rates_origin=$rates @endphp
<!--
@for($i=0 ; $i<$message_count ; $i++)
{{$rates[$i]}}
@endfor
-->
@php $message_ordered=$messages @endphp
@php $collection = collect() @endphp

@for($i=0 ; $i<$message_count ; $i++)
@for($j=0 ; $j<$message_count ; $j++)
@if($rates[$i]>$rates[$j])
@php $store=$rates[$i] @endphp
@php $rates[$i]=$rates[$j] @endphp
@php $rates[$j]=$store @endphp
@php $store1=$message_ordered[$i] @endphp
@php $message_ordered[$i]=$message_ordered[$j] @endphp
@php $message_ordered[$j]=$store1 @endphp
@endif
@endfor
@endfor

@php $collection=collect($rates) @endphp
<!--{{$collection}}-->
    @if (count($messages) > 0)
        <div class="panel panel-default, center ">
            <div class="panel-body">
                <table class="table table-striped message-table">
                    <!-- Table Headings -->
                    <!-- Table Body -->
                    <tbody>
			<div class="panel-body">
			    <i class="fa fa-sort-numeric-asc" aria-hidden="true" style="font-size:20px"> 依評分排序</i>
			</div>
			@php $a=0 @endphp
                        @foreach ($message_ordered as $message)
			    <tr>
                                <!-- Message Name -->
                                <td class="table-text">
					<i class="fa fa-hashtag" aria-hidden="true" style="float:left; padding-top:0%"><b><font size=6>{{$a+1}}</font></b></i>
					<a href="{{ url('messages/'.$message->id) }}" style="float:left; padding-left:2%"><div><font size=5>{{$message->name}}</font></div></a>
					@if($rates[$a]==0)
					<p style="float:left; color:red; padding-left:1%">未評分</p>
					@else
					<p style="float:left; color:red; padding-left:1%">{{sprintf("%.1f",($rates_origin[$message_ordered[$a]->id-1]*10)/10)}}</p>
					@endif
					@php $a+=1 @endphp
                                </td>
                                <!-- Delete Button -->
                                <td align=right>
                                    @if (Auth::user()->id==$message->user_id || Auth::user()->id==1)
                                    <form action="{{ url('message/'.$message->id) }}" method="POST">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}
					<div style="padding-left:800px">
                                        <button type="submit" id="delete-message-{{ $message->id }}" class="btn btn-danger">
                                            <i class="fa fa-btn fa-trash"></i>刪除
                                        </button>
					</div>
                                    </form>
                                    @endif
                                </td>
				<!-- Edit Button -->
				<td align=right>
				    @if (Auth::user()->id==$message->user_id)
	 	 		      <form action="{{ url('messages/'.$message->id.'/edit') }}" method="GET">
					<button type="submit" id="edit-message-{{ $message->id }}" class="btn btn-info">
			                	<i class="fa fa-pencil fa-fw"></i>編輯
					</button>
   				     </form>
   				     @endif
				</td>
				<!-- more comment Button -->
				<td align=right>
	 	 		      <form action="{{ url('messages/'.$message->id.'/notes') }}" method="POST">
	    	   			<button type="submit" id="edit-message-{{ $message->id }}" class="btn btn-info">
			                	問答區 <i class="fa fa-comments-o"></i>
       				     	</button>
   				     </form>
				</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection
