@extends('layouts.app')
@section('content')
<div class="col-md-6 col-md-offset-3">
        
<form action="{{ url('messages/report_back') }}" method="POST" class="form-horizontal">
{{ method_field('POST') }}
<div class="form-group">
	    <label for="message-name" class="col-sm-3 control-label">檢舉原因:   </label>
	    <div class="form-group">
		<textarea name="reason" class="form-control" rows="10" cols="10"></textarea>
    	    </div>
</div>

<div style="display:none">
<textarea name="url" class="form-control">{{ $url }}</textarea>
</div>

<div class="form-group" style='text-align:center'>
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> 送出
                    </button>
                </div>
            </div>
</form>
<div style='text-align:right'>
	<form action="{{ url('messages/report/back') }}" method="ANY" class="form-horizontal">

<div style="display:none">
<textarea name="url" class="form-control">{{ $url }}</textarea>
</div>

		<button type="submit" id="back" class="btn btn-default">
			回上一頁 <i class="fa fa-undo"></i>
		</button>
	</form>
</div>
@endsection