@extends('layouts.app')
@section('content')
<div class="col-md-6 col-md-offset-3">
<form action="{{ url('messages/'.$message->id.'/update') }}" method="POST">
	    	   			
{{ csrf_field() }}
            <!-- 標題 -->
            <div class="form-group">
                <label for="message-name" class="col-sm-3 control-label">標題: </label>
                <div class="col-sm-6">
                    <textarea name="name" class="form-control">{{ $message->name }}</textarea>
                </div>
            </div>

	    <!--內文-->
<div class="form-group">
<br></br>
<br></br>
	    <label for="message-name" class="">內文:   </label>
	    <div class="form-group">
                <textarea name="content" class="form-control">{{ $message->content }}</textarea>
            </div>
</div>

            <!-- 更改按鈕-->
            <div class="form-group" style='text-align:center'>
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> 更改
                    </button>
                </div>
            </div>

        </form>

<div style='text-align:right'>
	<form method="ANY" action="/messages/back">
		<button type="submit" id="back" class="btn btn-default">
			回上一頁 <i class="fa fa-undo"></i>
		</button>
	</form>
</div>
@endsection