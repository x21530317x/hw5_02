@extends('layouts.app')
@section('content')
<div class="col-md-6 col-md-offset-3">
@if(isset($empty))
    <div class="alert alert-danger">
            <li>標題和內文為必填</li>
    </div>
@endif
<form action="{{ url('message') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <!-- 標題 -->
            <div class="form-group">
                <label for="message-name" class="col-sm-3 control-label">標題: </label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="message-name" class="form-control">
                </div>
            </div>

	    <!--內文-->
<div class="form-group">
	    <label for="message-name" class="col-sm-0 control-label">內文:   </label>
	    <div class="form-group">
		<textarea name="content" id="name" class="form-control" rows="10" cols="10" wrap="physical"></textarea>
    	    </div>
</div>

        <div class="">
            <label class="col-sm-2 col-form-label" for="image">圖片 :</label>
                <div class="col-sm-10">
                    <input class="form-control" type="file" name="image">
                </div>
                {{--  <img src="http://via.placeholder.com/1200x600" class="mt-3" style="height: 100%; width: 100%; object-fit: contain">  --}}
        </div>

            <!-- 增加留言按鈕-->
            <div class="form-group" style='text-align:center; padding-top:8%'>
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> 貼文
                    </button>
                </div>
            </div>

        </form>

<div style='text-align:right'>
	<form method="ANY" action="/messages/back">
            {{ csrf_field() }}
		<button type="submit" id="back" class="btn btn-default">
			回上一頁 <i class="fa fa-undo"></i>
		</button>
	</form>
</div>
@endsection