@extends('layouts.app')
@section('content')
<div class="col-md-6 col-md-offset-3">
    <div class="breadcrumb">
<h1><center>{{ $message->name }}</center></h1>
<h4 style="padding-left:20px"><B>{{$message->content}}</B></h4>
<p style="padding-left:70%"> 發布者: {{$message->user->name}} </p>
<p style="padding-left:70%"> 發布於: {{$message->created_at}} </p>
    </div>
@if(isset($empty))
@if ($empty==1)
    <div class="alert alert-danger">
            <li>未輸入評論</li>
    </div>
@endif
@endif
    <ul class="list-group">
        @foreach ($message->notes as $note)
	@if ($note->body != null)

        <li class="list-group-item" style="color:blue">
	<pre>
{{$note->body}}										By {{$note->name}}
							   留言時間: {{ $note->updated_at}}</pre>
	</li>
        @endif
	@endforeach
    </ul>

<div style='text-align:center'>
    <form method="POST" action="/messages/{{ $message->id}}/notes/store">
        {{ csrf_field() }}
        <div class="form-group">
            <textarea name="body" class="form-control"></textarea>
        </div>
<div style="display:none">
<textarea name="name" class="form-control">{{ Auth::user()->name }}</textarea>
</div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">新增留言</button>
        </div>
    </form>
</div>
<div style='text-align:right'>
	<form method="ANY" action="/messages/back">
		<button type="submit" id="back" class="btn btn-default">
			回上一頁 <i class="fa fa-undo"></i>
		</button>
	</form>
</div>
	</div>
</div>
@endsection