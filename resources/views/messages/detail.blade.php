@extends('layouts.app')
@section('content')
<div class="col-md-6 col-md-offset-3">
@php $num=0 @endphp
@foreach($message->stars as $star)
@if($message->id == $star->message_id && Auth::user()->id==$star->follower_id)
@php $num = $star->stars @endphp
@endif
@endforeach

@php $num_count=0 @endphp
@php $total=0 @endphp
@foreach($message->stars as $star)
@if($message->id == $star->message_id)
@php $total+=$star->stars @endphp
@php $num_count+=1 @endphp
@endif
@endforeach
@if($num_count==0)
@php $rate='未評分' @endphp
@else
@php $rate = $total/$num_count @endphp
@php $rate=floor($rate*100)/100 @endphp
@endif
    <div class="breadcrumb">
<h1><center><B>{{ $message->name }}</B></center></h1>
<h3 style="padding-left:10px"><B>{{$message->content}}</B></h3>
@if(is_file('./images/'.$message->image) && $message->image!='default.png')
<h4 style="padding-left:10px"><B>圖片:</B></h4>
<img src="{{asset('./images/'.$message->image)}}" width = "100%" height = "100%" style="border-radius: 50% 20% / 10% 40%;">
@endif
<p style="float:left; padding-left:55%"> 按讚次數: {{$message->like_count}} </p>
<p style="float:left; padding-left:55%"> 關注人數: {{count($message->followers)}} </p>
<p style="padding-left:70%"> 討論度: {{count($message->notes)}} </p>
<p style="padding-left:70%"> 評分: {{$rate}}</p>
<p style="float:left; padding-left:55%"> 發布者: {{$message->user->name}} </p>
<p style="padding-left:70%"> 發布於: {{$message->created_at}} </p>
    </div>
<!--
@if(is_file('./images/default.png'))
<p>yes</p>
@else
<p>no</p>
@endif
-->
@if (Auth::user()->id==$message->user_id)
	@if(count($message->followers) != 0)
	<h3>關注此商品的用戶 :</h3>
	@endif
        @foreach ($message->followers as $follower)
        <li class="list-group-item" style="">
	<pre>
<h2>{{$follower->follower_name}}</h2>
							關注時間: {{ $follower->updated_at}}</pre>
	</li>
	@endforeach
@endif
@if (Auth::user()->id!=$message->user_id)
<div style="float:left; padding-left:75%">
<div style="float:left"><form action="{{ url('messages/'.$message->id.'/star') }}" method="POST">
<div style="display:none">
<textarea name="star" class="form-control">1</textarea>
<textarea name="user_id" class="form-control">{{Auth::user()->id}}</textarea>
<textarea name="message_id" class="form-control">{{$message->id}}</textarea>
</div>
    <button type="submit" id="star">
	@if($num >= 1)
	<i class="fa fa-star"></i>
	@else
	<i class="fa fa-star-o"></i>
	@endif
    </button>
</form>
</div>
<div style="float:left"><form action="{{ url('messages/'.$message->id.'/star') }}" method="POST">
<div style="display:none">
<textarea name="star" class="form-control">2</textarea>
<textarea name="user_id" class="form-control">{{Auth::user()->id}}</textarea>
<textarea name="message_id" class="form-control">{{$message->id}}</textarea>
</div>
    <button type="submit" id="star">
	@if($num >= 2)
	<i class="fa fa-star"></i>
	@else
	<i class="fa fa-star-o"></i>
	@endif
    </button>
</form></div>

<div style="float:left"><form action="{{ url('messages/'.$message->id.'/star') }}" method="POST">
<div style="display:none">
<textarea name="star" class="form-control">3</textarea>
<textarea name="user_id" class="form-control">{{Auth::user()->id}}</textarea>
<textarea name="message_id" class="form-control">{{$message->id}}</textarea>
</div>
    <button type="submit" id="star">
	@if($num >= 3)
	<i class="fa fa-star"></i>
	@else
	<i class="fa fa-star-o"></i>
	@endif
    </button>
</form></div>

<div style="float:left"><form action="{{ url('messages/'.$message->id.'/star') }}" method="POST">
<div style="display:none">
<textarea name="star" class="form-control">4</textarea>
<textarea name="user_id" class="form-control">{{Auth::user()->id}}</textarea>
<textarea name="message_id" class="form-control">{{$message->id}}</textarea>
</div>
    <button type="submit" id="star">
	@if($num >= 4)
	<i class="fa fa-star"></i>
	@else
	<i class="fa fa-star-o"></i>
	@endif
    </button>
</form></div>

<div style="float:left"><form action="{{ url('messages/'.$message->id.'/star') }}" method="POST">
<div style="display:none">
<textarea name="star" class="form-control">5</textarea>
<textarea name="user_id" class="form-control">{{Auth::user()->id}}</textarea>
<textarea name="message_id" class="form-control">{{$message->id}}</textarea>
</div>
    <button type="submit" id="star">
	@if($num >= 5)
	<i class="fa fa-star"></i>
	@else
	<i class="fa fa-star-o"></i>
	@endif
    </button>
</form></div>
</div>
@endif
<div style = "float:left; padding-left:75%; padding-top:2%">
    @php $flag = 1 @endphp
    @foreach($message->likes as $value)
        @if ($value->follower_id == Auth::user()->id)
	    @php $flag = 0 @endphp
        @endif
    @endforeach

    @if (Auth::user()->id!=$message->user_id)
        @if ($flag==1) 
        <form action="{{ url('messages/'.$message->id.'/like') }}" method="POST">
	    <button type="submit" id="like" class="btn btn-danger">
 		喜歡 <i class="fa fa-thumbs-o-up"></i>
	    </button>
        </form>
        @else
	<form action="{{ url('messages/'.$message->id.'/unlike') }}" method="POST">
	    <button type="submit" id="unlike" class="btn btn-success">
 		喜歡 <i class="fa fa-thumbs-up"></i>
	    </button>
        </form>
	@endif
    @endif
</div>

<div style = "float:left; padding-left:10px; padding-top:2%">
    @php $flag2 = 1 @endphp
    @foreach($message->followers as $value)
        @if ($value->follower_id == Auth::user()->id)
	    @php $flag2 = 0 @endphp
        @endif
    @endforeach

    @if (Auth::user()->id!=$message->user_id)
        @if ($flag2==1) 
	<form action="{{ url('messages/'.$message->id.'/follower') }}" method="POST">
	    <button type="submit" id="like" class="btn btn-info">
		關注 <i class="fa fa-bell-o"></i>
       	    </button>
    	</form>
        @else
	<form action="{{ url('messages/'.$message->id.'/unfollower') }}" method="POST">
	    <button type="submit" id="like" class="btn btn-success">
		關注 <i class="fa fa-bell"></i>
       	    </button>
    	</form>
	@endif
    @endif
</div>
@if (Auth::user()->id!=$message->user_id)
@php $flag_reason = 0 @endphp
<div style = "float:left; padding-left:76%; padding-top:10px">
	<form action="{{ url('messages/'.$message->id.'/report') }}" method="POST">
@if (session('reason'))
@php $flag_reason = 1 @endphp
<div style="display:none">
<textarea name="reason" class="form-control">{{session('reason')}}</textarea>
</div>
@else
<div style="display:none">
<textarea name="reason" class="form-control">null</textarea>
</div>
@endif
	@if($flag_reason==1)
	    <button type="submit" id="report" class="btn btn-success">
	@else
	    <button type="submit" id="report" class="btn btn-info">
	@endif
		檢舉 <i class="fa fa-bell-o"></i>
       	    </button>
    	</form>
</div>
<div style = "float:left; padding-left:10px; padding-top:10px">
	<form action="{{ url('messages/report') }}" method="POST">
	    <button type="submit" id="report_reason" class="btn btn-info">
		檢舉原因 
       	    </button>
    	</form>
</div>
@endif
<div style="float:left; padding-left:88.5%; padding-top:10px">
	<form method="ANY" action="/messages/back">
		<button type="submit" id="back" class="btn btn-default">
			回上一頁 <i class="fa fa-undo"></i>
		</button>
	</form>
</div>


</div>
@endsection