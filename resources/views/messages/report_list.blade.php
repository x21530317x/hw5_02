@extends('layouts.app')
@section('content')
<div class="col-md-6 col-md-offset-3">

@php $flag=0 @endphp
    <ul class="list-group">
        @foreach ($report as $i)
        <li class="list-group-item" style="color:blue">
<pre>
<u>{{$reporter[$flag]}}</u> 檢舉了 <u>{{$reported[$flag]}}</u> 的貼文: <a herf="{{ url('messages/'.$message_id[$flag]) }}">{{$message_name[$flag]}}</a>
@if($i!='null')  檢舉原因:
<div style="padding-left:8%; font-size:20px">{{$i}}</div>@endif
<div style="float:left; padding-left:87%"><form action="{{ url('messages/show2') }}" method="POST" class="form-horizontal"><div style="display:none"><textarea name='{{'message_id'}}' class="form-control">{{ $message_id[$flag] }}</textarea></div><button type="submit" id="back" class="btn btn-success">文章連結</button></form></div>
<div style="float:left; padding-left:67%">檢舉時間: {{$create_at[$flag]}}</div>
</pre>
	</li>
	@php $flag+=1 @endphp
	@endforeach
    </ul>

<div style='text-align:right'>
	<form action="{{ url('messages/back') }}" method="ANY" class="form-horizontal">
		<button type="submit" id="back" class="btn btn-default">
			回上一頁 <i class="fa fa-undo"></i>
		</button>
	</form>
</div>
@endsection