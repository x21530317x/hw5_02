@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">歡迎!!</div>

                <div class="panel-body">
                    歡迎來到二手商品交換網!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
