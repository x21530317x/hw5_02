<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\Note;
class NotesController extends Controller
{
    public function store(Request $request, Message $message)
    {
        if($request->body=="")
	{
	    $empty=1;
            return view('messages.show',compact('message','empty'));
	}
        $message->notes()->create([
            'body' => $request->body,
	    'name' => $request->name
        ]);
        return view('messages.show',compact('message'));
    }
    public function show(Message $message)
    {
        return view('messages.show',compact('message'));
    }
}