<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\Like;
class LikesController extends Controller
{
    public function like(Request $request, Message $message)
    {
	$count=$message->like_count;
	$count+=1;
	$message->update([
                'like_count' => $count
        ]);
        $message->likes()->create([
        'follower_id' => $request->user()->id,
        'LikeOrNot' => 1
        ]);
        return back();
    }

    public function unlike(Request $request, Message $message)
    {
	$count=$message->like_count;
	$count-=1;
	$message->update([
                'like_count' => $count
        ]);
        $message->likes()->delete();
        return back();
    }
}
