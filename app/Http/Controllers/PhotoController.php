<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PhotoController extends Controller
{
    public function photo(Request $request, Message $message)
    {
        if (!file_exists('/images')) {
            mkdir('/images', 0755, true);
        }
	if($request->hasFile('image')){
	    $file = $request->file('image');
	    $path = public_path().'\images\\';
	    $fileName=().'.'.$file->getClientOriginalExtension();
	    $file->move($path,$fileName);
	}
	else{
	    $fileName='default.jpg';
	}
	$message->photos()->create([
            'path' => $fileName
        ]);
	return back();
    }
}
