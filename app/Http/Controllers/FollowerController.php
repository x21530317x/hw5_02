<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\Follower;
class FollowerController extends Controller
{
    public function follow(Request $request, Message $message)
    {
        $message->followers()->create([
        'follower_id' => $request->user()->id,
        'follower_name' => $request->user()->name
        ]);
        return back();
    }
    public function unfollow(Request $request, Message $message)
    {
        $message->followers()->delete();
        return back();
    }
}
