<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\Star;

class StarController extends Controller
{
    public function star(Request $request, Message $message)
    {
	/*return $message->stars->where('message_id','=',$request->message_id)->first()->follower_id;*/
	/*
	$total=0;
	$count=0;
	for($i=0;$i<count($message->stars->where('message_id','=',$request->message_id));$i++)
	{
		$total+=$message->stars->where('message_id','=',$request->message_id)[$i]->stars;
		$count+=1;
	}
	$rate=$total/$count;
	$message->all()->where('id','=',$request->message_id)[1]->update([
                'rate' => floor($rate*100)/100
        ]);*/
	$flag1=0;
	$flag2=0;
	for($i=0;$i<count($message->stars->where('message_id','=',$request->message_id));$i++)
	{
		if($request->user_id == $message->stars->where('message_id','=',$request->message_id)[$i]->follower_id)
		$flag1=1;
	}
	for($i=0;$i<count($message->stars->where('message_id','=',$request->message_id));$i++)
	{
		if($request->message_id == $message->stars->where('message_id','=',$request->message_id)->first()->message_id)
		$flag2=1;
	}
	if($flag1==1 && $flag2==1)
	{
		$message->stars()->where([
		['message_id','=',$request->message_id],
		['follower_id','=',$request->user_id],
		])->update([
        	'stars' => $request->star
        	]);
	}
	else
	{
		$message->stars()->create([
        	'follower_id' => $request->user()->id,
        	'stars' => $request->star
        	]);
	}
	return back();
    }
}
