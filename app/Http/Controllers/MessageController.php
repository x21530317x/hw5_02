<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\Repositories\MessageRepository;
class MessageController extends Controller
{
    //
    public function __construct(MessageRepository $messages)
    {
        $this->middleware('auth');
        $this->messages = $messages;
    }
    /**
     * Display a list of all of the user's message.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index_self(Request $request)
    {
        $messages = $request->user()->messages()->get();
        return view('messages.index', [
            'messages' => $messages,
        ]);
    }
    /**
     * Display a list of all users' messages.
     * 
     * @param Request  $request
     * @return Response 
     */
    public function index()
    {
        $messages = Message::all();
        return view('messages.index', [
            'messages' => $messages,
        ]);
    }
    /**
     * Create a new message.
     *
     * @param  Request  $request
     * @return Response
     */
    public function add(Message $message){
        return view('messages.add',compact('message'));
    }
    public function store(Request $request)
    {	
	if (!file_exists('./images')) {
            mkdir('./images', 0755, true);
        }
	if($request->name == "" || $request->content=="")
	{
	    $empty=1;
	    return view('/messages/add',compact('empty'));
	}
	if($request->hasFile("image")){
	    $file=$request->file('image');
	    $path='./images';
	    $image = $request->file('image')->storeAs('avatars', $request->user()->id);
	    /*$extension = $file->getClientOriginalExtension();*/
	    $fileName = time().'.'.$file->getClientOriginalExtension();
	    $file->move($path,$fileName);
	    /*$file->save();*/
	}
	else{
	    $fileName='default.png';
	}

	
	$zero=0;
        $this->validate($request, [
            'name' => 'required|max:255',
            'content' => 'required'
        ]);
	/*$content=$request->content.replace('\n','<br/>');*/
 	$content=str_replace("\n","<br/>",$request->content);
        $request->user()->messages()->create([
            'name' => $request->name,
            'content' => $request->content,
	    'image'=>$fileName,
	    'like_count' => $zero,
	    'rate'=>$zero
        ]);
        return redirect('/messages');
    }
    /**
     * Destroy the given message.
     *
     * @param  Request  $request
     * @param  Message  $message
     * @return Response
     */
    public function destroy(Request $request, Message $message)
    {
        /*$this->authorize('destroy', $message);
        */$message->delete();
        return redirect('/messages');
    }
    /**
     * Return message details
     * 
     * @param Request  $request
     * @return Response
     */
    public function show(Message $message)
    {
        return view('messages.detail',compact('message'));
	/*
        return sprintf("title: %s<br></br> create by: %s.<p></p>recently update at: %s"
        ,$message->name,$message->user->name,$message->updated_at);
	*/
    }

    public function show2(Message $message, Request $request)
    {
	$message = $message->all()->where('id', '=', $request->message_id);
	$message = $message[1];
        return view('messages.detail',compact('message'));
    }

        //$message = Message::find($id);
	/*
        return sprintf("title: %s<br></br> create by: %s.<p></p>recently update at: %s"
        ,$message->name,$message->user->name,$message->updated_at);*/
        /*created_at
        updated_at*//*
        return view('/messages.show',[
            '/message'=>$message
        ]);*/
    public function back(){
        return redirect('/messages');
    }
    public function edit(Message $message)
    {
        return view('messages.edit',compact('message'));
    }
    public function update(Request $request, Message $message)
    {
        /*$this->authorize('update', $message);*/
        $this->validate($request, [
            'name' => 'required|max:255',
            'content' => 'required'
        ]);
        
        $message->update([
                'name' => $request->name,
                'content' => $request->content
        ]);
        return redirect('/messages');
    }
    public function like(Request $request, Message $message)
    {
	$count=$message->like_count;
	$count+=1;
	$message->update([
                'like_count' => $count
        ]);
	return redirect('/messages/'.$message->id);
	/*
	return sprintf("%s給%s喜歡"
        ,$request->user()->name,$message->user->name);
	*/
    }
    public function photo(Request $request, Message $message)
    {
	return redirect('/messages.imageUpload');
    }
    public function report(Message $message, Request $request)
    {
	$url=redirect()->back()->getTargetUrl();
	return view('/messages.report',compact('url'));
    }
    public function report_submit(Message $message, Request $request)
    {
	$message->reports()->create([
        'reporter' => $request->user()->name,
        'reported' => $message->user->name,
	'reason' => $request->reason,
	'message_name' => $message->name
        ]);
	return back();
    }
    public function report_list(Message $message, Request $request)
    {
	/*$count=$message->all()->count();
	$a = $message->all()->where('id', '=', 1)->reports;*/

	$report=array("$request->report1");
	$create_at=array("$request->create_at1");
	$reporter=array("$request->reporter1");
	$reported=array("$request->reported1");
	$message_id=array("$request->message_id1");
	$message_name=array("$request->message_name1");
	for($i = 2; $i <= $request->count ; $i++)
	{
	    $reason="report".$i;
	    $time="create_at".$i;
	    $reporter_add="reporter".$i;
	    $reported_add="reported".$i;
	    $message_id_add="message_id".$i;
	    $message_name_add="message_name".$i;

	    $report = array_pad($report, $i, $request->$reason); 
	    $create_at = array_pad($create_at, $i, $request->$time);
	    $reporter=array_pad($reporter,$i,$request->$reporter_add);
	    $reported=array_pad($reported,$i,$request->$reported_add);
	    $message_id=array_pad($message_id,$i,$request->$message_id_add);
	    $message_name=array_pad($message_name,$i,$request->$message_name_add);
	}
	/*return $report;*/
	return view('/messages.report_list',compact('report','create_at','reporter','reported','message_id','message_name'));
    }
}
/*
<img src="{{ asset('images/' . $message->image) }}" class="mt-3" style="height: 100%; width: 100%; object-fit: contain" onerror="this.src='{{ asset('images/default.jpg') }}'">
index.blade 傳所有值給controller(檔名用陣列往上)($message->name<a></a>) 再給report.blade 顯示 user1 檢舉了user2的 $message_id 文章(redirect('用message_id導超連結'))
=======
}
/*
每個message對應一個使用者建立 follow
存 
按讚?
追蹤 => 作者可看到
<img src="{{ asset('images/' . $message->image) }}" class="mt-3" style="height: 100%; width: 100%; object-fit: contain" onerror="this.src='{{ asset('images/default.jpg') }}'">

>>>>>>> 713573c8141c08ba199a3a47b4798eb44fe30b86
*/