<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Star extends Model
{
    protected $fillable = ['follower_id','stars'];
    public function message()
    {
        return $this->belongsTo(Message::class);
    }
}
