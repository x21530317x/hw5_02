<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Message;
class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function destroy(User $user, Message $message)
    {
        return $user->id === $message->user_id;
    }
    public function update(User $user, Message $message){ 
        return $user->id === $message->user_id;
    }
    public function like(User $user, Message $message){ 
        return $user->id === $message->user_id;
    }
    public function unlike(User $user, Message $message){ 
        return $user->id === $message->user_id;
    }
}
