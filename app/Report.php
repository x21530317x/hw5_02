<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['reporter','reported','reason','message_name'];
    public function message()
    {
        return $this->belongsTo(Message::class);
    }
}
