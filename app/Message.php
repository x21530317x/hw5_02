<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    
    protected $fillable = ['name','content','like_count','image','rate'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
	public function notes()
	{
	    return $this->hasMany(Note::class); // Note::class=App\Note
	}
	public function followers()
	{
	    return $this->hasMany(Follower::class);
	}
	public function likes()
	{
	    return $this->hasMany(Like::class);
	}
	public function stars()
	{
	    return $this->hasMany(Star::class);
	}
	public function photos()
	{
	    return $this->hasMany(Photo::class);
	}
	public function reports()
	{
	    return $this->hasMany(Report::class);
	}
}
