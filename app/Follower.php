<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    protected $fillable = ['follower_id','follower_name'];
    public function message()
    {
         return $this->belongsTo(Message::class);
    }
}
