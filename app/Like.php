<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['follower_id','LikeOrNot'];
    public function message()
    {
        return $this->belongsTo(Message::class);
    }
}
