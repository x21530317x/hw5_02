<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});
Route::get('/admin', function() {
    return view('admin');
});
Route::get('/register', 'Auth\AuthController@register')->name('register');
Route::post('/register', 'Auth\AuthController@storeUser');

Route::get('/login', 'Auth\AuthController@login')->name('login');
Route::post('/login', 'Auth\AuthController@authenticate');
Route::get('logout', 'Auth\AuthController@logout')->name('logout');

Route::get('/password/reset', 'Auth\AuthController@passwordReset');

Route::get('/home', 'Auth\AuthController@home')->name('home');



Route::get('messages', 'MessageController@index');
Route::post('message', 'MessageController@store');
Route::post('messages/add', 'MessageController@add');
Route::delete('message/{message}', 'MessageController@destroy');

Route::any('messages/back', 'MessageController@back');

Route::get('messages/{message}', 'MessageController@show');
Route::post('messages/{message}/notes', 'NotesController@show');
Route::post('messages/{message}/notes/store', 'NotesController@store');
Route::get('messages/{message}/edit','MessageController@edit');
Route::post('messages/{message}/update', 'MessageController@update');
 
Route::post('messages/{message}/like', 'LikesController@like');
Route::post('messages/{message}/unlike', 'LikesController@unlike');
Route::post('messages/{message}/follower', 'FollowerController@follow');
Route::post('messages/{message}/unfollower', 'FollowerController@unfollow');
Route::post('messages/{message}/star', 'StarController@star');

Route::get('/showInfo', 'InformationController@showInfo');
Route::post('/postInfo', 'InformationController@postInfo');

Route::get('messages/{message}/upload', 'PhotoController@photo');
Route::get('image-upload', 'ImageUploadController@imageUpload')->name('image.upload');
Route::post('image-upload', 'ImageUploadController@imageUploadPost')->name('image.upload.post');

Route::post('messages/report_list','MessageController@report_list');/*show report list to admin*/
Route::post('messages/{message}/report','MessageController@report_submit');
Route::any('messages/report/back','ReportsController@back');/*back from report to message*/
Route::post('messages/report_back','ReportsController@report');/*send data back to message*/
Route::post('messages/report','MessageController@report');/*redirect to report.blade*/
Route::post('messages/show2','MessageController@show2');